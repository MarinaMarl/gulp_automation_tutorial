// Gulpfile

// First we load the plugins using Node.js and its require function:

var gulp = require('gulp');


// We will create a simple task to get familiar with basic methods. We will take index.html and copy it to the folder assets. The code for that is:

gulp.task('copy', function() {
  gulp.src('index.html')
  .pipe(gulp.dest('assets'))
});
// Most important methods of gulp object are:

// src where we put the name of the file we want to work with and use as an input,
// pipe will take output of the previous command as pipe it as an input for the next,
// dest writes the output of previous commands to the folder we specify.

// Here we called plug-in we defined as gutil, and called its log method with our custom message. Now we can run log task in the terminal: gulp log
var gutil = require('gulp-util');

gulp.task('log', function() {
  gutil.log('== My Log Task ==')
})

// Now we will create a task to process the styles/main.scss which can be then used in our HTML file.

// To do this just follow the established pattern: load the plug-in and create the task:

var sass = require('gulp-sass');

gulp.task('sass', function() {
  gulp.src('styles/main.scss')
  .pipe(sass({style: 'expanded'}))
    .on('error', gutil.log)
  .pipe(gulp.dest('assets'))
});
// Here we piped main.scss file to sass object and as an argument we passed an object with options we want to use.
// Visit official Sass reference for more information about output styles, and gulp-sass plug-in page for some other options.

// We also handled a possible error message and used gulp-util to log it.

// compiles coffee script into simple javascript. coffeescript is the equivalent of what scss is to css.
var coffee = require('gulp-coffee');

gulp.task('coffee', function() {
  gulp.src('scripts/hello.coffee')
  .pipe(coffee({bare: true})
    .on('error', gutil.log))
  .pipe(gulp.dest('scripts'))
});

// Asset compilation is an act of minifying and concatenating scripts together so that the server loads the page faster.

// In this task we will minify all the JavaScript files using gulp-uglify plug-in, and then merge them using gulp-concat.

var uglify = require('gulp-uglify'),
    concat = require('gulp-concat');

gulp.task('js', function() {
  gulp.src('scripts/*.js')
  .pipe(uglify())
  .pipe(concat('script.js'))
  .pipe(gulp.dest('assets'))
});
// We combined two tasks in an array, in the order we want them executed. Now run default task and both tasks will execute one after the other:

// gulp
gulp.task('default', ['coffee', 'js']);

// watch method comes as a standard method of the gulp object, so theres not need to require it
gulp.task('watch', function() {
  gulp.watch('scripts/hello.coffee', ['coffee']);
  gulp.watch('scripts/*.js', ['js']);
  gulp.watch('styles/main.scss', ['sass']);
});

// Creating Server for Live Reload:
// First we will load gulp-connect and create a new task:

var connect = require('gulp-connect');

gulp.task('connect', function() {
  connect.server({
    root: '.',
    livereload: true
  })
});
// visit gulp-connect https://www.npmjs.com/package/gulp-connect for more information

// Complete Integration:
// To be able to see the changes we make to source files, we need to reload the server after there is a change.
// It means that we need to establish a relation between watch and connect, and we need to pipe the end of some tasks to connect.reload().
// go to the actual gulpfile.js to see how it all bands together!
